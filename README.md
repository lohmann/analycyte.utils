
# analycyte.utils

<!-- badges: start -->
<p align="center" style="display: flex; align-items: center;vertical-align: middle; ">
<a href="https://CRAN.R-project.org/package=analycyte.utils">
<img src="https://www.r-pkg.org/badges/version/analycyte.utils" alt="CRAN status" style="margin-right: 5px;">
</a> <a href="https://lifecycle.r-lib.org/articles/stages.html#stable">
<img src="https://img.shields.io/badge/lifecycle-stable-brightgreen.svg" alt="Lifecycle: stable">
</a>
</p>
<!-- badges: end -->

The goal of `analycyte.utils` is to contain all the useful functions of
the shiny `analycyte` package.

## Installation

You can install the development version of `analycyte.utils` like so:

``` r
remotes::install_git("https://gitcrcm.marseille.inserm.fr/lohmann/analycyte.utils")
```

## Code of Conduct

Please note that the `analycyte.utils` project is released with a
[Contributor Code of
Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.
