% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/sort_factor.R
\name{sort_factor}
\alias{sort_factor}
\title{Sort a SE factor of interest}
\usage{
sort_factor(
  to_sort_table = NULL,
  col_interest = NULL,
  col_cell = NULL,
  sens = "desc",
  personnalized = NULL,
  is_ordered = F,
  ...
)
}
\arguments{
\item{to_sort_table}{table to sort with this function}

\item{col_interest}{column with factor to sort}

\item{col_cell}{a sorting column complementary to the column of interest}

\item{sens}{desc or asc. default value to desc.}

\item{personnalized}{presorted vector value}

\item{is_ordered}{boolean, wheter the factor is ordered or not. default value to FALSE.}

\item{...}{see getFeature() parameters.}
}
\value{
order a vector of sorted factors.
}
\description{
Usage: use for ggplot2 purposes.
}
\examples{
# sort_factor()
}
