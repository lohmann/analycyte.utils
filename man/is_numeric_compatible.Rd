% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/is_numeric_compatible.R
\name{is_numeric_compatible}
\alias{is_numeric_compatible}
\title{is_numeric_compatible}
\usage{
is_numeric_compatible(column)
}
\arguments{
\item{column}{column name}
}
\value{
a boolean
}
\description{
is_numeric_compatible
}
\examples{
df <- data.frame(
  ID = 1:3,
  Name = as.character(c("Alice", "Bob", "Charlie")),
  Age = as.character(c("25", "30", "35")),
  Gender = as.factor(c("Female", "Male", "Male")),
  stringsAsFactors = FALSE
)

is_numeric_compatible(df$Age) # TRUE

numeric_compatible <- sapply(df, is_numeric_compatible)
}
