# analycyte.utils (development version)

### 3.0.2

- add heatmap_pres_abs() function
- Correction pca quantitative variable scaling 
- avoid factorisation of all annotations in heatmap 


### 3.0.11

- General version 3.0.11 accross analycyte-verse 
- correction of heatmap when subsetting to 1 element with drop=FALSE

### 3.0.10

- Update boxplot_report_figure
- Update mfi heatmap for DS

